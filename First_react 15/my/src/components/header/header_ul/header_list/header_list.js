import React from 'react';

import '../header_list/header_list.css';

const HeaderList = () => {
    return (
        <>
            <li><a href="#">Home</a></li>
            <li><a href="#">Photoapp</a></li>
            <li><a href="#">Design</a></li>
            <li><a href="#">Download</a></li>
        </>
        
    )
} 
export default HeaderList;